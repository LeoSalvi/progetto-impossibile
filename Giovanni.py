import time
import subprocess
import argparse


def funzione_error(mapping):
  aligned=[]
  felix_link=[]
  for i, x  in enumerate(mapping[:-1]):
    if x[0] == "lpGBT1":
      optical_link="00"
    elif x[0] == "lpGBT2":
      optical_link="01"
    elif x[0] == "lpGBT3":
      optical_link="02"
    elif x[0] == "lpGBT4":
      optical_link="03"
    else:
      print("Check mapping!")
    egroup = x[1]
    optical_link=str(optical_link)
    aligned_link="ALIGNED_" + optical_link
    aligned_link=str(aligned_link)
    aligned_aux = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
    aligned_aux = subprocess.Popen(["grep", aligned_link], stdin=aligned_aux.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4][-(int(egroup)+1)]
    felix_link.append("LINK_" + optical_link + "_ERRORS_EGROUP" + str(egroup))
    aligned.append(aligned_aux)
  results=[0,0,0,0]
  subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
  start_time = time.time() 
  while time.time()-start_time < 10:
    if aligned==["0", "0", "0", "0"]:
      print('ATTENZIONE!! ATTENZIONE!! qualcosa non va')
      break    
    for i in range(4):
	# aligned = [1,1,1,0]
      if aligned[i]!="0":
        output=subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
        output=subprocess.Popen(["grep", felix_link], stdin=output.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4]
        results[i]+=int(output, 16)
      else:
        results[i]=-1
    time.sleep(2)
    subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
  return results

board={"DP1":[ ["lpGBT2" , "0"] , ["lpGBT2" , "1"] , ["lpGBT2" , "2"] , ["lpGBT2" , "3"] , "Downlink3" ],
      "DP2":[ ["lpGBT1" , "0"] , ["lpGBT1" , "1"] , ["lpGBT1" , "2"] , ["lpGBT1" , "3"] , "Downlink0" ],
      "DP3":[ ["lpGBT1" , "0"] , ["lpGBT1" , "1"] , ["lpGBT3" , "2"] , ["lpGBT3" , "3"] , "Downlink1" ],
      "DP4":[ ["lpGBT2" , "0"] , ["lpGBT2" , "1"] , ["lpGBT4" , "2"] , ["lpGBT4" , "3"] , "Downlink2"  ],
      "DP5":[ ["lpGBT4" , "0"] , ["lpGBT4" , "1"] , ["lpGBT4" , "2"] , ["lpGBT4" , "3"] , "Downlink7"  ],
      "DP6":[ ["lpGBT3" , "0"] , ["lpGBT3" , "1"] , ["lpGBT3" , "2"] , ["lpGBT3" , "3"] , "Downlink5" ],
      }

parser = argparse.ArgumentParser(description="funzione errori")
parser.add_argument("-dp", "--display_port",type=str, default="DP1", choices=["DP1","DP2","DP3","DP4","DP5","DP6",], required=True, help="display_port number for soft_error test with 6dp-to-erf8 adapter board" )
args = vars(parser.parse_args())
dp=args["display_port"]
mapping=board[dp]

funzione_error(mapping)
