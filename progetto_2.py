def softErrorCounter(self, optical_link, egroup, meastime):
        """The number of soft errors is monitored by checking the associated felix register.
        `flx-reset SOFT_RESET` command is called after each readout of the felix register therefore this method CANNOT BE CALLED IF FELIXCORE IS RUNNING!

        Args:
            optical_link (str): optical link to monitor, example: "00" for felix channel 0
            egroup (int): lpGBT egroup to monitor
            meastime (int): measuring time in seconds
        Returns:
            List with the measured errors, to be summed to have the total amount of errors
        """

        logger.warning("Turn off felixcore")
        aligned_link = "ALIGNED_" + optical_link
        felix_link = "LINK_" + optical_link + "_ERRORS_EGROUP" + str(egroup)
        start_time = time.time()
        counter = []
        subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
        aligned = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
        aligned = subprocess.Popen(["grep", aligned_link], stdin=aligned.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4][-(egroup+1)]

        while (time.time()-start_time < meastime):

            if (aligned!="1"): 
                logger.info("Lost decoding alignment! Skipping to next setting!")
                counter.append(-1)
                break

            try:
                output = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
                output = subprocess.Popen(["grep", felix_link], stdin=output.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4]
            except:
                logger.error("Readout of FELIX register failed!") 
                raise Exception("Readout of FELIX register failed!")
            if (output!="0x0"):
                counter.append(int(output,16))
            #if (output=="0xf"):
                subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
            time.sleep(2)

        return counter

